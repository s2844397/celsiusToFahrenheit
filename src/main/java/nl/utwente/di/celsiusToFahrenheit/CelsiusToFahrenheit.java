package nl.utwente.di.celsiusToFahrenheit;

import java.io.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;


public class CelsiusToFahrenheit extends HttpServlet {

    private Converter converter;

    public void init() {
        converter = new Converter();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType = "<!DOCTYPE HTML>\n";
        String title = "Celsius To Fahrenheit Converter";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Celsius: " +
                request.getParameter("celsius") + "\n" +
                "  <P>Fahrenheit: " +
                Double.toString(converter.getFahrenheit(Double.parseDouble(request.getParameter("celsius")))) +
                "</BODY></HTML>");
    }

}