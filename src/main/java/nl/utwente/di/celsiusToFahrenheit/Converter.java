package nl.utwente.di.celsiusToFahrenheit;

public class Converter {
    public double getFahrenheit(double celsius){
        return celsius*1.8 + 32;
    }
}
